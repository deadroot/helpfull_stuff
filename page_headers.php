<?php session_start(); ?><!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>header checker</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  </head>
  <body>

      <?php
        $url = "";
        if(!empty($_REQUEST["url"])){
           $url = $_REQUEST["url"];
        }

        $methode = "get";
        if(!empty($_REQUEST["methode"])){
           $methode = $_REQUEST["methode"];
        }
      ?>

      <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Header checker</a>
    </div>

  </div><!-- /.container-fluid -->
</nav>

      <div class="container">
      <form method="GET">
          <div class="form-group">
           <div class="input-group">
             <div class="input-group-addon">Url</div>
             <input type="text" class="form-control" name="url" value="<?php echo $url; ?>" placeholder="Url" />
             <span class="input-group-btn">
                 <input type="submit" class="btn btn-primary" value="Go">
             </span>
           </div>
          </div>

          <div class="form-group row">
              <label class="col-sm-2 control-label">Request-Type: </label>
                <div class="col-sm-10">
                    <label class="radio-inline">
                        <input type="radio" name="methode" <?php echo ($methode == "get"? 'checked="checked"': ''); ?> value="get"> GET
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="methode" <?php echo ($methode == "post"? 'checked="checked"': ''); ?> value="post"> POST
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="methode" <?php echo ($methode == "put"? 'checked="checked"': ''); ?> value="put"> PUT
                    </label>
                </div>
          </div>

      </form>
      </div>

      <?php
            if(!empty($_SESSION["last_check"]) && $_SESSION["last_check"] > time() - 5){
                $url = "";

                echo '<div class="container">'
                    . '<div class="alert alert-danger" role="alert">Fuck you! W8 5 sec!</div>'
                    . '</div>';
            } else {
                $_SESSION["last_check"] = time();
            }
          ?>


      <?php if($url){ ?>
      <div class="container">

          <?php
            $time_start = microtime(TRUE);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);

            if($methode == "post"){
                curl_setopt($ch, CURLOPT_POST, true);
            } elseif ($methode == "put"){
                curl_setopt($ch, CURLOPT_PUT, true);
            }

            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $time_end = microtime(TRUE);
            $time_diff = ($time_end - $time_start);

            list($header, $body) = explode("\r\n\r\n", $response, 2);
         ?>

          <div class="panel panel-default">
            <div class="panel-heading">Info</div>
            <div class="panel-body">
              <?php
                echo "<ul>";
                echo "<li><strong>Url:</strong> ".$url."</li>";
                echo "<li><strong>Load time:</strong> ".$time_diff." sek</li>";
                echo "<li><strong>Http-Status:</strong> ".$httpcode."</li>";
                echo "</ul>";
              ?>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">Curl header</div>
            <div class="panel-body">
              <?php
                echo "<ul>";
                foreach (explode("\r\n", $header) as $i => $line)
                        {
                            list ($key, $value) = explode(': ', $line);
                            echo "<li><strong>{$key}:</strong> {$value}</li>";
                        }
                echo "</ul>";
              ?>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">$_SERVER header</div>
            <div class="panel-body">
                <pre>
                    <?php
                        print_r($_SERVER);
                    ?>
                </pre>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">Content</div>
            <div class="panel-body">
                <pre>
                    <?php
                        echo htmlspecialchars($body);;
                    ?>
                </pre>
            </div>
          </div>

      </div>
      <?php } ?>



    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

  </body>
</html>
